#include "fondation.h"
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int my_connect(char *ip, int port)
{
	int sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock == INVALID_SOCKET)
	{
		perror("socket()");
		my_put_nbr(errno);
	}

struct hostent *hostinfo = NULL;
SOCKADDR_IN sin = { 0 };
//char *hostname = ip;
hostinfo = gethostbyname(ip);
if (hostinfo == NULL)
{
	my_putstr("Unknow host:\n");

	return (EXIT_FAILURE);
}

sin.sin_addr = *(IN_ADDR *) hostinfo->h_addr;
sin.sin_port = htons(port);
sin.sin_family = AF_INET;

if(connect(sock, (SOCKADDR *) &sin, sizeof(SOCKADDR)) == SOCKET_ERROR)
{
	perror("connect()");
	return (errno);
}
	my_putstr("\n connection to ");
	my_putstr(ip);
	my_putstr(" is ok \n\n");
	return (sock);
};

