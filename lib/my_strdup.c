/*
** my_strdup.c for my_strdup in /home/vagrant/projects/piscine/devc/jour07/diallo_l/my_strdup
**
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
**
** Started on  Mon Oct 27 16:40:36 2014 DIALLO Leny
** Last update Mon Oct 27 16:40:38 2014 DIALLO Leny
*/

#include <stdlib.h>

int       my_strlen(char*);
char      *my_strcpy(char*, char*);

char      *my_strdup(char *str)
{
  int     len;
  char    *dup;

  len = my_strlen(str);
  dup = malloc(len * sizeof(char));
  return (my_strcpy(dup, str));
}
