/*
** my_strcpy.c for my_strcpy in /home/vagrant/projects/piscine/devc/jour04/diallo_l/my_strcpy
**
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
**
** Started on  Thu Oct 23 09:14:20 2014 DIALLO Leny
** Last update Thu Oct 23 09:14:23 2014 DIALLO Leny
*/

char      *my_strcpy(char *dest, char *src)
{
  char    *ret;

  ret = dest;
  while (*src)
  {
    *dest = *src;
    dest++;
    src++;
  }
  *dest = '\0';
  return (ret);
}
