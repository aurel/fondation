/*
** my_strstr.c for my_strstr in /home/vagrant/projects/piscine/devc/jour04/diallo_l/my_strstr
**
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
**
** Started on  Thu Oct 23 11:34:23 2014 DIALLO Leny
** Last update Thu Oct 23 11:34:25 2014 DIALLO Leny
*/

int       check_needle(char *hay, char *needle)
{
  while (*needle)
  {
    if (*needle != *hay)
      return (1);
    needle++;
    hay++;
  }
  return (0);
}

char      *my_strstr(char *str, char *to_find)
{
  if (*to_find == '\0')
    return (str);
  while (*str)
  {
    if (*str == *to_find && check_needle(str, to_find) == 0)
    {
      return (str);
    }
    str++;
  }
  return ("null");
}
