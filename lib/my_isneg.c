/*
** my_isneg.c for my isneg in /home/vagrant/projects/piscine/devc/jour01/diallo_l/my_isneg
** 
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
** 
** Started on  Mon Oct 20 14:10:46 2014 DIALLO Leny
** Last update Mon Oct 20 14:30:32 2014 DIALLO Leny
*/

int	my_isneg(int n)
{
  if (n >= 0)
    return (1);
  return (0);
}
