/*
** my_aff_tab.c for my aff tab in /home/vagrant/projects/piscine/devc/jour02/diallo_l/my_aff_tab
** 
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
** 
** Started on  Tue Oct 21 09:16:21 2014 DIALLO Leny
** Last update Tue Oct 21 15:44:33 2014 DIALLO Leny
*/
#include <unistd.h>

void	my_putstr(char *str);
void	my_put_nbr(int n);
int	calc_diviser(int n)
{
  int	diviser;

  diviser = 1;
  while (n > 10)
    {
      n = n / 10;
      diviser = diviser * 10;
    }
  return (diviser);
}

void	my_aff_tab(int *tab, int len)
{
  int	i;

  i = 0;
  while (i < len)
    {
      my_put_nbr(*tab);
      write(1, "\n", 1);
      tab++;
      i++;
    }
}
