/*
** my_swap.c for my swap in /home/vagrant/projects/piscine/devc/jour03/diallo_l/my_swap
**
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
**
** Started on  Wed Oct 22 12:00:36 2014 DIALLO Leny
** Last update Wed Oct 22 12:01:43 2014 DIALLO Leny
*/

void    my_swap(int *a, int *b)
{
  int   tmp;

  tmp = *b;
  *b = *a;
  *a = tmp;
}
