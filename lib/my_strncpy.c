/*
** my_strncpy.c for my_strncpy in /home/vagrant/projects/piscine/devc/jour04/diallo_l/my_strncpy
**
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
**
** Started on  Thu Oct 23 09:14:20 2014 DIALLO Leny
** Last update Mon Jan 26 10:17:27 2015 Pigot Aurélien
*/

char      *my_strncpy(char *dest, char *src, int n)
{
  char    *ret;
  int     i;

  i = 0;
  ret = dest;
  while (*src && i < n)
  {
    *dest = *src;
    src++;
    dest++;
    i++;
  }
  while (i < n)
  {
    dest ="\0";
    dest++;
    i++;
  }
  return (ret);
}
