/*
** my_putnbr_base.c for my putnbr base in /home/vagrant/projects/piscine/devc/jour05/diallo_l
**
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
**
** Started on  Fri Oct 24 14:12:04 2014 DIALLO Leny
** Last update Fri Oct 24 14:12:06 2014 DIALLO Leny
*/
#include <unistd.h>

void    my_putstr(char *str);

int     getbaselen(char  *base)
{
  int   i;

  i = 0;
  while (*base)
  {
    base++;
    i++;
  }
  return (i);
}

char*    put_base(int number, char *base)
{
  char   c;

  c = base[number];
  write(1, &c, 1);
  return (base);
}

int     calc_div(int n, char *base)
{
  int   diviser;
  int   base_len;

  diviser = 1;
  base_len = getbaselen(base);
  while ((n / diviser) >= base_len)
    {
      diviser = diviser * base_len;
    }
  return (diviser);
}

void    my_putnbr_base(int n, char *base)
{
  int   diviser;
  int   base_len;
  char  c;

  if (n < 0)
    {
      n = n * -1;
      write(1, "-", 1);
    }
  diviser = calc_div(n, base);
  base_len = getbaselen(base);
  while (diviser > 0)
    {
      c = (n / diviser) % base_len;
      put_base(c, base);
      diviser = diviser / base_len;
    }
}
/*#include <stdio.h>*/

/*int     main()*/
/*{*/
  /*int   number = 6;*/
  /*char  *base1 = "0123456789";*/
  /*char  *base2 = "01";*/
  /*char  *base3 = "0123456789ABCDEF";*/

  /*my_putnbr_base(number, base1);*/
  /*printf("\n");*/
  /*printf("\n");*/

  /*number = 6;*/
  /*my_putnbr_base(number, base2);*/
  /*printf("\n");*/
  /*printf("\n");*/

  /*number = 6;*/
  /*my_putnbr_base(number, base3);*/
  /*printf("\n");*/
  /*return (0);*/
/*}*/
