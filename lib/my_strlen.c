/*
** my_strlen.c for my strlen in /home/vagrant/projects/piscine/devc/jour03/diallo_l/my_strlen
**
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
**
** Started on  Wed Oct 22 12:06:30 2014 DIALLO Leny
** Last update Wed Oct 22 12:09:40 2014 DIALLO Leny
*/

int   my_strlen(char *str)
{
  int i;

  i = 0;
  while (*str != '\0')
    {
      i++;
      str++;
    }
  return (i);
}
