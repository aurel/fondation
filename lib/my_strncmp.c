/*
** my_strncmp.c for my_strncmp in /home/vagrant/projects/piscine/devc/jour04/diallo_l/my_strncmp
**
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
**
** Started on  Thu Oct 23 09:55:51 2014 DIALLO Leny
** Last update Thu Oct 23 09:55:52 2014 DIALLO Leny
*/

int       my_strncmp(char *s1, char *s2, int n)
{
  while (*s1 && n > 0)
  {
    if (*s1 < *s2)
      return (-1);
    else if (*s1 > *s2)
      return (1);
    s1++;
    s2++;
    n--;
  }
  if (n == 0 || (*s1 == '\0' && *s2 == '\0'))
    return (0);
  else
    return (-1);
}
