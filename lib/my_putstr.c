/*
** my_putstr.c for my putstr in /home/vagrant/projects/piscine/devc/jour02/diallo_l/my_putstr
** 
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
** 
** Started on  Tue Oct 21 10:38:15 2014 DIALLO Leny
** Last update Tue Oct 21 10:38:48 2014 DIALLO Leny
*/

#include <unistd.h>

void	my_putstr(char *str)
{
  while (*str != '\0')
    {
      write(1, str, 1);
      str++;
    }
}
