/*
** my_strcmp.c for my_strcmp in /home/vagrant/projects/piscine/devc/jour04/diallo_l/my_strcmp
**
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
**
** Started on  Thu Oct 23 09:43:03 2014 DIALLO Leny
** Last update Thu Oct 23 09:43:05 2014 DIALLO Leny
*/

int       my_strcmp(char *s1, char *s2)
{
  while (*s1)
  {
    if (*s1 < *s2)
      return (-1);
    else if (*s1 > *s2)
      return (1);
    s1++;
    s2++;
  }
  if (*s1 == '\0' && *s2 == '\0')
    return (0);
  else
    return (-1);
}
