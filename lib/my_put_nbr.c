/*
** my_put_nbr.c for my nb len in /home/vagrant/projects/piscine/devc/jour01/diallo_l/my_put_nbr
** 
** Made by DIALLO Leny
** Login   <diallo_l@etna-alternance.net>
** 
** Started on  Mon Oct 20 14:36:12 2014 DIALLO Leny
** Last update Mon Oct 20 21:48:57 2014 DIALLO Leny
*/
#include <unistd.h>

void	my_putstr(char *str);
int	calc_nbr_diviser(int n)
{
  int	diviser;

  diviser = 1;
  while ((n / diviser) >= 10)
    {
      diviser = diviser * 10;
    }
  return (diviser);
}

void	my_put_nbr(int n)
{
  int	diviser;
  char	c;

  if (n == -2147483648)
    {
      my_putstr("-2147483648");
      return;
    }
  if (n < 0)
    {
      n = n * -1;
      write(1, "-", 1);
    }
  diviser = calc_nbr_diviser(n);
  while (diviser > 0)
    {
      c = n / diviser + 48;
      n = n % diviser;
      diviser = diviser / 10;
      write(1, &c, 1);
    }
}
