/***
** fondation.c
** Last update Mon Jan 26 18:51:04 2015 Pigot Aurélien
****/
#include "fondation.h"
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int my_recv (int sock)
{
	int retour;
	char buffer[256];

	bzero(buffer, 256);

	retour = read(sock, buffer, 255);
	if (retour < 0)
	{
		perror("Error read()");
		return (errno);
	}else
	{
	my_putstr(buffer);
	my_putstr("\n");
	bzero(buffer,256);
	}
	return (1);
}