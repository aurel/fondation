/***
** fondation.c
** Last update Mon Jan 26 18:51:04 2015 Pigot Aurélien
****/
#include "fondation.h"
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv)
{
  char *option;
  char *entry;
  int readEntry;
  int i;\
  char *val;
  SOCKET sock;

  if(argc != 5){
    my_putstr("Pleas use commandes --ip[ip] --port[port] \n");
    return (0);
  }
    for(i = 1; argv[i]; i = i + 2 ){
      option = argv[i];
      val = "q";
      if(argv[i+1])
	       val = argv[i+1];
      if (read_option(option, val) != 1 ){
        my_putstr("\e[31m\n is not a valid option. pleas use --ip[ip] --port[port]\n\n\e[0m");
          return (0);

     }
    }
   
    // my_putstr("Prompt-> ");
    // entry = readLine();
    // readEntry = read_entry(entry);
    // free(entry);
    sock = my_connect(argv[2], my_getnbr(argv[4]));
    do
    {
      my_putstr("Prompt-> ");
      entry = readLine();
      readEntry = read_entry(entry);
      if (readEntry == -1)
      {
        my_putstr("\nyou are living terminus \n");
        close(sock);
        free(entry);
        return (0);
      }
      if(readEntry == 0)
      {
        free(entry);
        error_entry_message();
      }else{
        
        my_send(sock, entry);
        my_recv(sock);
        //close(sock);
        free(entry);
      }

    }while(readEntry != -1);
    free(entry);
  return (1);
}

