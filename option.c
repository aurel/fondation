/***
** option.c
** Last update Mon Jan 26 18:51:04 2015 Pigot Aurélien
****/
#include "fondation.h"
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int if_null_entry(char *entry);

char        *readLine()
{
  ssize_t   ret;
  char      *buff;

  if ((buff = malloc(sizeof(*buff) * (250 + 1))) == NULL)
    return (NULL);
  if ((ret = read(0, buff, 250)) > 1)
  {

    buff[ret - 1] = '\0';
    return (buff);
  }
  buff[0] = '\0';
  return (buff);
}

void error_entry_message()
{
   my_putstr(" \e[31m\nInvalid input\nplease type a valid command type usage for show commande\n\n\e[0m");
    my_putchar('\n');
}

int    read_entry(char *rln)
{
  int i;
  char *commands[] = {"/bye", "/auth", "/list", "/hs", "/psychohistory", 
                      "/getstatus", "/me", "/setstatus"};
  int returns[] = {-1, 1, 2, 3, 4, 5, 6, 7};
  char **wordtab;
  char *tmp;
  if(if_null_entry(rln) == 0){
	my_putstr("<<<<< Entry is NULL safe exit >>>>>\n");
	return (-1);
  }
  my_strtolower(rln);
  tmp = rln;
  wordtab = my_str_to_wordtab(tmp);
  printf("%s\n",rln );
  for (i = 0; i < 8; ++i){
    if (my_strcmp(wordtab[0], commands[i]) == 0)
      //free(wordtab);
      return (returns[i]);
  }
  if (my_strcmp(my_strupcase(rln), "USAGE") == 0)
     {
         printf("<<<<< Usages here >>>>>\n");
         return (8);
     }
   //error_entry_message();
     free(wordtab);
  return (0);
}


int    read_option(char *argoption, char *argval)
{
  if (isoption_ok(argoption, argval) == 1)
    {
      /*
       * remplire la struct de connection avec les data ok
       * et lancer la connection
       */
      return (1);
    }else
    return (0);


}

int    isoption_ok(char *option, char *val)
{
  char *validOption[] = {
    "--ip", "--port"
  };
  //int i;
  int j;
  //my_putstr(option);
  my_putstr(val);

  j = 0;
  while(j < 2){
    //printf("\navant strcmp option\n");
    // exit(0);
    printf("\n option is %s and valid option is %s \n ", option, validOption[j]);
    if(my_strcmp(validOption[j], option) != 0){
       my_putstr("Option is not valid \n");
       j++;
       continue;}
    if(my_strcmp(validOption[j], option) == 0)
      {
        my_putstr("Option is valide \n");
	//check the option value
        // for(i = 0; val[i]; i++ ){
        //   if(char_isnum(val[i]) == 0)
        //     {
        //       if(val[i] != '.'){
        //       my_putstr("Value is not valid ... \n");
        //       return (0);}
        //     }
        // }
        // my_putstr("Value is valid \n");
        return (1);
	}
    j++;
  }
  my_putstr("Option is not valid end \n");
  return (0);
}

int     char_isnum(char c)
{
  if (c >= '0' && c <= '9')
    return (1);
  else
    return (0);
}

int if_null_entry(char *entry){
  if(entry == NULL)
    return (0);
  else
    return (1);
}
