#ifdef WIN32 /* si vous êtes sous Windows */

#include <winsock2.h> 

#elif defined (linux) /* si vous êtes sous Linux */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h> /* close */
#include <netdb.h> /* gethostbyname */
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define PORT 4848
#define closesocket(s) close(s)
typedef int SOCKET;
typedef struct sockaddr_in SOCKADDR_IN;
typedef struct sockaddr SOCKADDR;
typedef struct in_addr IN_ADDR;

#else /* sinon vous êtes sur une plateforme non supportée */

#error not defined for this platform

#endif

typedef struct t_opt t_opt;
typedef struct t_list t_list;


struct t_opt
{
  char *key;
  char *val;
  t_opt *next;
};       

struct t_list
{
	t_opt *first;
};

void    my_putchar(char c);

void    my_put_nbr(int n);

void    my_putstr(char *str);

int 	my_getnbr(char *str);

char    *readLine();

char    *my_strupcase(char *str);

char    *my_strtolower(char *str);

char      **my_str_to_wordtab(char *str);

int     my_strcmp(char *s1, char *s2);

int     read_entry(char *rln);

void   error_entry_message();

int    read_entry(char *rln);

int    isoption_ok(char *option, char *val);

int    read_option(char *option, char *val);

int    char_isnum(char c);

int    my_connect(char *ip, int port);

int    add_node(t_list *list, char *key, char *val);

int    my_send(int sock, char *com);

int    my_recv (int sock);