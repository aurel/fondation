/***
** fondation.c
** Last update Mon Jan 26 18:51:04 2015 Pigot Aurélien
****/
#include "fondation.h"
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

int    my_send(int sock, char *com)
{
	int n;

	n = write(sock, com, strlen(com));

	if(n < 0)
	{
	    perror("send()");
	    return (errno);
	}
	return (1);
}

