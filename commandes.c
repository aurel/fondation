/***
** commandes.c
** Last update Mon Jan 26 18:51:04 2015 Pigot Aurélien
****/
#include "fondation.h"
#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

t_list *make_struct()
{
	t_opt *element;
	t_list *list;

	list = malloc(sizeof(*list));
	element = malloc(sizeof(*element));
	if (list == NULL || element == NULL)
	{
		exit (EXIT_FAILURE);
	}

	element->key = "nullKey";
	element->val = "nullVal";
	element->next = NULL;
	list->first = element;

	return (list);

}

char *find_in_node(t_opt *structure, char *key)
{
	while(structure != NULL)
	{
		if (structure->key == key)
			return (structure->val);
	}
	return (0);
}

int add_node(t_list *list, char *key, char *val)
{
	t_opt *node;

	node = malloc(sizeof(*node));
	if (list == NULL || node == NULL)
	{
		return (EXIT_FAILURE);
	}
	node->key = key;
	node->val = val;
	node->next = list->first;
	list->first = node;
	return (1);
}

int del_list(t_list *list)
{
	t_opt *to_del;

	if (list == NULL)
		return (EXIT_FAILURE);

	if (list->first != NULL)
	{
		to_del = list->first;
		list->first = list->first->next;
		free(to_del);
		return (1);
	}
	return (0);
}

int print_list(t_list *list)
{
	if (list == NULL)
		return (EXIT_FAILURE);

	t_opt *view = list->first;

	while (view != NULL)
	{
		my_putstr("\nKey is: ");
		my_putstr(view->key);
		my_putstr("\n");
		my_putstr("\nVal is: ");
		my_putstr(view->val);
		my_putstr("\n");

		view = view->next;

	}
	my_putstr("NULL\n");
	return (1);
}

void free_struct(t_opt *structure)
{
	t_opt *temp;

	temp = malloc(sizeof(temp));
	while (structure != NULL)
	{
		temp = structure;
		structure = structure->next;
		free(temp);
	}
}